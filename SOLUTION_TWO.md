
# Solution Two

The wolf bug is caused by the method that determines the path of the wolf not being thread-safe.
In order to fix this bug, we need to make the method thread-safe.
As such, by adding the synchronised keyword to the method, it forces the program to wait until one call of the method is finished before it allows another.
By doing it this way, it allows this solution to be scalable, as it doesn't depend on an arbitrary wait time like solution one did.