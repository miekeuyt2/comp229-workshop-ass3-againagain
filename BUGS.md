
# Bugs



  * The Rabbit Bug

  Upon hovering the mouse over the rabbit, the rest of the game freezes while the rabbit figures out its path.
This is because the code which determines the path of the rabbit is in the same thread as the one that runs the rest of the game, and so when it calls for the thread to sleep, everything sleeps while it computes the path.

  * The Wolf Bug

When you look at the determined path for the wolf, it gives us two separate paths immediately after one another. From this we can see that this is probably a multithreading issue, with the method to determine the path not being thread-safe.

