
# Solution One

The wolf bug is caused by the method that determines the path of the wolf not being thread-safe.
In order to fix this bug, we can make it so after each call to the Run method of the thread, it waits a certain amount of time to ensure that there are not multiple calls running at the same time.

As such, by adding a Thread.sleep call after the end of the run method of the thread, we can force the method to wait before allowing a new call to run.
This, however, is a bit of a cheap fix, as it delays loading time considerably, and it is not scalable.