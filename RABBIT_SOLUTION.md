
# Rabbit Solution

The rabbit bug is caused by the code which determines the rabbit's path being within the same thread as the game screen. As such, when the rabbit's method calls Thread.sleep, everything will sleep until the timer ends. In order to fix this bug, we must make the rabbit's code run in a different thread.
I have managed to successfully do this, but currently the grid is being repainted over the top of the painted determined path.
In order to fix this I must make the paint be done outside of the thread, and the determination of the path be within the thread.